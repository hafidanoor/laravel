<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(){
        return view('form');
    }

    public function kirim(Request $request){
        $fname = $request['fname'];
        $lname = $request['lname'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $bahasa = $request['bahasa'];
        $eng = $request['eng'];
        $other3 = $request['other3'];
        $bio = $request['bio'];

        return view('welcome', compact('fname','lname'));
    }
}
