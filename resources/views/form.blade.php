<!DOCTYPE html>
<html>
    <head>
        <title>Buat Account</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/kirim" method="POST">
            @csrf
            <label for="fname">First name:</label><br><br>
            <input type="text" id="fname" name="fname"><br><br>
            <label for="lname">Last name:</label><br><br>
            <input type="text" id="lname" name="lname"><br><br>
            <p>Gender:</p>
            <input type="radio" id="male" name="gender" value="Male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="Female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="Other">
            <label for="other">Other</label><br><br>
            <label for="nationality">Nationality:</label><br><br>
            <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="british">British</option>
            <option value="other2">Other</option><br>
            </select>
            <p>Language Spoken:</p>
            <input type="checkbox" id="bahasa" name="bahasa" value="Bahasa">
            <label for="bahasa">Bahasa Indonesia</label><br>
            <input type="checkbox" id="eng" name="eng" value="English">
            <label for="eng">English</label><br>
            <input type="checkbox" id="other3" name="other3" value="Other">
            <label for="other3">Other</label><br><br>
            <label for="bio">Bio:</label><br>
            <textarea id="bio" name="bio"></textarea><br><br>
            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>