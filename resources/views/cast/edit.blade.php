@extends('layout.master')

@section('title')
Detail Cast ke- {{$cast->id}}
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="title">Nama Cast</label>
        <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan Nama Cast">
        @error('nama')
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Umur Cast</label>
        <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="umur" placeholder="Masukkan Umur Cast">
        @error('umur')
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Bio Cast</label>
        <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="bio" placeholder="Masukkan Bio Cast">
        @error('bio')
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>

@endsection